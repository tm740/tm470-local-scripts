import json
import http.client
import sys
import numpy as np

file_to_process=sys.argv[1]


#source: https://stackoverflow.com/questions/11763976/python-http-client-json-request-and-response-how
#Access date: 25/8/18

def loadData(file_to_process):
	with open(file_to_process) as f:
		data = json.load(f)
		return data

def geoQueryPostcodes(postcodes, filename):
    connection = http.client.HTTPSConnection('api.postcodes.io')
    headers = {'Content-type': 'application/json'}

    x=0
    y=50
    n=50
    data_dict={}
    while x < len(postcodes):
        print('Querying for {} to {}'.format(x,y))
        data = {"postcodes" : postcodes[x:y]}
        json_data = json.dumps(data)
        print('Querying {} postcodes..'.format(len(postcodes)))
        connection.request('POST', '/postcodes', json_data, headers)
        response = connection.getresponse()
        print(response.status)
        response_json = json.loads(response.read().decode())
        for entry in response_json['result']:
            if entry['result'] == None:
                continue
        
            postcode = entry['query']
            lat = entry['result']['latitude']
            lng = entry['result']['longitude']
            data_dict[postcode] = {'lat': lat, 'lng': lng}

        x+=n
        y+=n

    np.save(filename+'.geo.npy', data_dict) 
    print('Done. {} entries saved to '.format(len(data_dict))+filename+'.geo.npy')
    return data


if __name__=='__main__':
    data_dict = loadData(file_to_process)
    postcodes=[]
    for k,v in data_dict.items():
        postcodes.append(data_dict[k]['postcode'])

    geoQueryPostcodes(postcodes, file_to_process)
    



