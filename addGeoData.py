import sys
import json
import time
import hashlib
import numpy as np

def loadGeo(file_to_read):
	return np.load(file_to_read).item()

def loadData(file_to_process):
	with open(file_to_process) as f:
		data = json.load(f)
		return data

if __name__=='__main__':
    file_to_process = sys.argv[1]
    geo_file = sys.argv[2]

    geo_data = loadGeo(geo_file)
    data = loadData(file_to_process)

    for k,v in data.items():
        data[k]['name'] = k
        postcode = data[k]['postcode']
        if not postcode in geo_data:
            continue
        lat = geo_data[postcode]['lat']
        lng = geo_data[postcode]['lng']
		
        data[k]['geo'] =  { 'x': lng, 'y': lat }
        string_to_hash = k+postcode
        _id = hashlib.md5(string_to_hash.encode()).hexdigest()
        data[k]['id'] = _id

    
    with open('FINAL_{}'.format(file_to_process), 'w') as fp:
        json.dump(data, fp)
    print('Data written to {}'.format('FINAL_'+file_to_process))
