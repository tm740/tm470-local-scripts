import sys
import os
import xml.etree.ElementTree as ET
import json

if __name__ == '__main__':
    dir_to_process = sys.argv[1]
    location_name = sys.argv[2]
    cp_dict = {}

    for f in os.listdir(dir_to_process):
        if f.endswith('.xml'):
            tree = ET.parse(os.path.join(dir_to_process,f))
            root = tree.getroot()
            

            for child in root:
                for child1 in child:
                    #print(child1.tag)
                    noSpaces =0
                    charges_dict = {}
                    if child1.tag.endswith("CarParkName"):
                        name = child1.text
                    elif child1.tag.endswith("Location"):
                        location = child1.text
                    elif child1.tag.endswith("Postcode"):
                        postcode = child1.text
                    if child1.tag.endswith("Notes"):
                       notes = child1.text
                    elif child1.tag.endswith("CarParkAdditionalData"):
                        for child2 in child1:
                            if child2.tag.endswith("CarParkSpace"):
                                for child3 in child2:
                                    if child3.tag.endswith("NumberOfSpaces"):
                                        if noSpaces < int(child3.text):
                                            noSpaces = int(child3.text)
                                    elif child3.tag.endswith("Charges"):
                                        timeRangeDays = 0
                                        timeRangeMins = 0
                                        chargeAmount = 0
                                        for child4 in child3:
                                            if child4.tag.endswith("TimeRangeDays"):
                                                timeRangeDays = int(child4.text)
                                            if child4.tag.endswith("TimeRangeMinutes"):
                                                timeRangeMins = int(child4.text)
                                            if child4.tag.endswith("ChargeAmount"):
                                                chargeAmount = int(child4.text)
                                            if timeRangeDays > 0:
                                                charges_dict[str(timeRangeDays)+" day(s)"] = chargeAmount
                                            if timeRangeMins > 0:
                                                charges_dict[str(timeRangeMins)+" mins"] = chargeAmount
                #if location == location_name:
                cp_dict[name] = {'postcode': postcode, 'notes': notes, 'location': location, 'spaces': noSpaces, 'cost': charges_dict}

    with open('result-{}.json'.format(location_name), 'w') as fp:
        json.dump(cp_dict, fp)
